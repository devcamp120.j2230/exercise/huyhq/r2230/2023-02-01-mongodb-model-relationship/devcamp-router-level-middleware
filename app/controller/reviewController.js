const { default: mongoose } = require('mongoose');

const reviewModel = require("../model/reviewModel");
const courseModel = require("../model/courseModel");

const getAllReviewOfCourse = (req, res) => {
    var courseId = req.params.courseId;

    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            message: "Error 400: Id khong dung dinh dang."
        })
    }

    courseModel.findById(courseId)
        .populate("review")                                                 //cho phép tìm các dữ liệu review tương ứng với id
        .exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `Error 500: ${error.message}`
                })
            } else {
                return res.status(200).json({
                    message: "Lay du lieu thanh cong.",
                    review: data.review,
                })
            };
        });
};

const getAReviewById = (req, res) => {
    var id = req.params.reviewId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Error 400: Id khong dung dinh dang."
        })
    }

    reviewModel.findById(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: "Lay du lieu thanh cong.",
                review: data,
            })
        };
    })
};

const postAReviewOfCourse = (req, res) => {
    var courseId = req.params.courseId;
    var body = req.body;

    if (!Number.isInteger(body.stars) || body.stars < 0 || body.stars > 5) {
        return res.status(400).json({
            message: "Error 400: star khong dung dinh dang."
        })
    }
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            message: "Error 400: Id khong dung."
        })
    };

    const newReview = new reviewModel({
        _id: mongoose.Types.ObjectId(),
        stars: body.stars,
        note: body.note,
    });

    reviewModel.create(newReview, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            courseModel.findByIdAndUpdate(courseId,
                {
                    $push: { review: data._id }                               //thêm review_id vào table course tương ứng
                },
                (error) => {
                    if (error) {
                        return res.status(500).json({
                            message: `Error 500: ${error.message}`
                        })
                    } else {
                        return res.status(201).json({
                            message: `Tao moi review thanh cong.`,
                            data
                        })
                    }
                }
            )

        }
    })

};

const putAReviewById = (req, res) => {
    var id = req.params.reviewId;
    var body = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Error 400: Id khong dung dinh dang."
        })
    }

    if (!Number.isInteger(body.stars) || body.stars < 0 || body.stars > 5) {
        return res.status(400).json({
            message: "Error 400: star khong dung dinh dang."
        })
    }

    const review = new reviewModel({
        stars: body.stars,
        note: body.note,
    });

    reviewModel.findByIdAndUpdate(id, review, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Sua thanh cong du lieu id = ${id}.`,
                review: data
            })
        };
    });
};

const deleteAReviewById = (req, res) => {
    var courseId = req.params.courseId;
    var reviewId = req.params.reviewId;

    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            message: "Error 400: courseId khong dung dinh dang."
        })
    };

    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return res.status(400).json({
            message: "Error 400: reviewId khong dung dinh dang."
        })
    }
    
    reviewModel.findByIdAndDelete(reviewId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            courseModel.findByIdAndUpdate(courseId,
                {
                    $pull: {review: reviewId}                               //xóa review_id trong bảng course tương ứng
                },
                (error) => {
                    if (error) {
                        return res.status(500).json({
                            message: `Error 500: ${error.message}`
                        })
                    }else{
                        return res.status(204).json({
                            message: `Xoa thanh cong id = ${reviewId}`
                        })
                    }
                }
            )
        }
    })
};

module.exports = {
    getAllReviewOfCourse,
    getAReviewById,
    postAReviewOfCourse,
    putAReviewById,
    deleteAReviewById
}