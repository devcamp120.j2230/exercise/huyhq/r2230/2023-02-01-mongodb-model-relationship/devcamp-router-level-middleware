const express = require("express");

const reviewRoute = express.Router();

const {
    getAllReviewOfCourse,
    getAReviewById,
    postAReviewOfCourse,
    putAReviewById,
    deleteAReviewById
} = require("../controller/reviewController");

reviewRoute.get("/course/:courseId/review", getAllReviewOfCourse);
reviewRoute.get("/review/:reviewId", getAReviewById);
reviewRoute.post("/course/:courseId/review", postAReviewOfCourse);
reviewRoute.put("/review/:reviewId", putAReviewById);
reviewRoute.delete("/course/:courseId/review/:reviewId", deleteAReviewById);

module.exports = {reviewRoute};

