const express = require("express");

//import middleware
const {
    getAllCourses,
    getACourse,
    postACourse,
    putACourse,
    deleteACourse,
} = require('../middlewares/courseMiddleware');

//import controller
const {
    getAllCoursesController,
    getACourseController,
    postACourseController,
    putACourseController,
    deleteACourseController
} = require('../controller/courseController')

const courseRouter = express.Router();

courseRouter.get('/course', getAllCourses, getAllCoursesController)

courseRouter.post('/course', postACourse, postACourseController)

courseRouter.get('/course/:id', getACourse, getACourseController)

courseRouter.put('/course/:id', putACourse, putACourseController)

courseRouter.delete('/course/:id', deleteACourse, deleteACourseController)

module.exports = {courseRouter};